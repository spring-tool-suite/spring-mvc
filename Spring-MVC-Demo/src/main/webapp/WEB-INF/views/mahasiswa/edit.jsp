<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	request.setAttribute("contextName",request.getContextPath());
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Mahasiswa</title>
</head>
<body>
	<p>Welcome to Spring MVC Tutorial</p>
	
	<form action="${contextName}/mhs" method="post">
		<input type="hidden" name="mode" value="edit">
		<input type="hidden" name="id" value="${mahasiswa.id}">
			NIM <input type="text" name="nim" value="${mahasiswa.nim}"><br>
			Nama <input type="text" name="nama" value="${mahasiswa.nama}"><br>
			Alamat <input type="text" name="alamat" value="${mahasiswa.alamat}"><br>
		<button type="submit">Simpan</button>
		
	</form>
	<form action="${contextName}/mhs" method="post">
		
		<input type="hidden" name="mode" value="hapus">
		<input type="hidden" name="id" value="${mahasiswa.id}">
		<button type="submit">Hapus</button>
	</form>
	
	<ol>
	<c:forEach var="mhs" items="${mhslist}">
	<li>${mhs.nim} - ${mhs.nama} (
		<a href="${contextName}/mhs/edit?id=${mhs.id}">Edit</a>
		)
	</li>
	</c:forEach>
	</ol>
	
</body>
</html>