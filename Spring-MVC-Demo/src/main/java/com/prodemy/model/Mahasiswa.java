package com.prodemy.model;

import lombok.Data;

@Data
public class Mahasiswa {
	
	private String id;
	private String nim;
	private String nama;
	private String alamat;
	
}
