package com.prodemy.dao;

import java.util.List;

import com.prodemy.model.Mahasiswa;

public interface MahasiswaDao {
	public Mahasiswa findById(String id) throws Exception;
	public void deleteById(String id) throws Exception;
	public void insert(Mahasiswa mhs) throws Exception;
	public void update(Mahasiswa mhs) throws Exception;
	public List<Mahasiswa> findAll() throws Exception;
}
